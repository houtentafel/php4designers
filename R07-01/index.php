<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
       $balance = 150;
        $value = 20;
        
        $newBalance = transferMoney($balance, $value);
        ?>
        <h1>Storting</h1>
        <p>
            <?php echo $value; ?> € storten op een rekening met <?php echo $balance; ?> € als saldo geeft een nieuw saldo van <?php echo $newBalance; ?>
        </p>
        
        <?php
        $balance = $newBalance;
        $value = 50;
        $newBalance = transferMoney($balance, $value, false);
        ?>
        
        <h1>Afhaling</h1>
        <p>
            <?php echo $value; ?> € afhalen van een rekening met <?php echo $balance; ?> € als saldo geeft een nieuw saldo van <?php echo $newBalance; ?>
        </p>
        
        <?php
        function transferMoney($balance, $value, $deposit=true) {
            $newBalance = $balance;
            
            if ($deposit === true) {
                $newBalance += $value;
            } else {
                $newBalance -= $value;
            }
            
            return $newBalance;
        }       
        ?>
    </body>
</html>
